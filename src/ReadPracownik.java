import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Scanner;
import java.util.StringTokenizer;

public class ReadPracownik {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        try(FileInputStream fis=new FileInputStream("pracownicy.ser");
        		ObjectInputStream ois=new ObjectInputStream(fis)){
        	Pracownik tmp;
        	while((tmp=(Pracownik)ois.readObject())!=null){
        		System.out.println(tmp.getImie()+" "+tmp.getNazwisko()+" "+
                		tmp.getDataUr().toString()+" "+tmp.getDataZatr().toString());
        	}        	
        } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        catch (EOFException e){
		  System.out.println("Koniec danych");	
		}
        catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

}
